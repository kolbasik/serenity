namespace Serenity.Server.Data
{
    public interface IDatabaseFactory
    {
        IDatabase CreateDatabase();
    }
}