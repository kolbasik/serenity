﻿using System;

namespace Serenity.Server.Data
{
    public interface IUnityOfWork : IDisposable
    {
        IDatabase Database { get; }
        IDatabase UseOnceTo();
    }
}