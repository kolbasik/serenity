using System;

namespace Serenity.Server.Data
{
    public class UnityOfWork : IUnityOfWork
    {
        private readonly IDatabaseFactory _databaseFactory;
        private readonly Lazy<IDatabase> _databaseLazy;

        public UnityOfWork(IDatabaseFactory databaseFactory)
        {
            if (databaseFactory == null) throw new ArgumentNullException("databaseFactory");
            _databaseFactory = databaseFactory;
            _databaseLazy = new Lazy<IDatabase>(databaseFactory.CreateDatabase, true);
        }

        public IDatabase Database
        {
            get { return _databaseLazy.Value; }
        }

        public IDatabase UseOnceTo()
        {
            return _databaseFactory.CreateDatabase();
        }

        public void Dispose()
        {
            if (_databaseLazy.IsValueCreated)
            {
                _databaseLazy.Value.Dispose();
            }
        }
    }
}