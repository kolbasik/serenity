﻿namespace Serenity.Server.Data
{
    public interface IQueryObject
    {
        void Execute(IDatabase database);
    }
}