﻿using System;
using System.Collections.Generic;
using Serenity.Server.Data.Entities;

namespace Serenity.Server.Data
{
    public interface IRepository<TEntity, in TKey> : IDisposable
        where TEntity : BaseEntity
    {
        IEnumerable<TEntity> Select(long offset, long limit);
        TEntity SelectOne(TKey id);
        bool InsertOrUpdate(TEntity entity);
        bool Delete(TEntity entity);
        void Commit();
    }
}