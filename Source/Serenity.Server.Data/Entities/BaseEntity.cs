﻿using System;

namespace Serenity.Server.Data.Entities
{
    public abstract class BaseEntity
    {
        protected BaseEntity()
        {
            IsActive = true;
        }

        public int Id { get; set; }
        public bool IsActive { get; set; }
        public DateTime CreatedOn { get; set; }
        public DateTime ModifiedOn { get; set; }
    }

    public class PersonEntity : BaseEntity
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
    }

    public class PaymentEntity : BaseEntity
    {
        public Guid PersonId { get; set; }
        public decimal Amount { get; set; }
    }
}
