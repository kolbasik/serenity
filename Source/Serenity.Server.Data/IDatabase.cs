﻿using System;
using System.Data;
using Serenity.Server.Data.Entities;

namespace Serenity.Server.Data
{
    public interface IDatabase : IDisposable
    {
        IRepository<TEntity, int> GetRep<TEntity>() where TEntity : BaseEntity;
        void BeginTransaction(IsolationLevel isolation = IsolationLevel.ReadCommitted);
        void CommitTransaction();
        void RollbackTransaction();
    }
}