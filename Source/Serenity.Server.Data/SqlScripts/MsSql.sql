﻿use Serenity

IF OBJECT_ID('[dbo].[Persons]') IS NOT NULL
	DROP TABLE [dbo].[Persons]

CREATE TABLE [dbo].[Persons]
(
	[Id] INT NOT NULL PRIMARY KEY IDENTITY, 
	[FirstName] NVARCHAR(50) NOT NULL DEFAULT '', 
	[LastName] NVARCHAR(50) NOT NULL DEFAULT '', 
	[CreatedOn] DATETIME NOT NULL DEFAULT getutcdate(), 
	[ModifiedOn] DATETIME NOT NULL DEFAULT getutcdate(), 
	[IsActive] BIT NOT NULL DEFAULT 1
)
