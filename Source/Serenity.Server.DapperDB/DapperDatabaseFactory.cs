﻿using System.Configuration;
using System.Data.Common;
using System.Data.SqlClient;
using Serenity.Server.Data;

namespace Serenity.Server.DapperDB
{
    public class DapperDatabaseFactory : IDatabaseFactory
    {
        const int CommandTimeout = 1;

        public IDatabase CreateDatabase()
        {
            var database = DapperDatabase.Init(CreateConnection(), CommandTimeout);
            database.BeginTransaction();
            return database;
        }

        public DbConnection CreateConnection()
        {
            return new SqlConnection(ConfigurationManager.ConnectionStrings["dapper"].ConnectionString);
        }
    }
}