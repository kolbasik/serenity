﻿using System;
using System.Collections.Generic;
using System.Linq;
using Serenity.Server.Data;
using Serenity.Server.Data.Entities;

namespace Serenity.Server.DapperDB
{
    public class DapperRepository<TEntity> : IRepository<TEntity, int>
        where TEntity : BaseEntity
    {
        private readonly DapperDatabase _database;
        private readonly DapperDatabase.Table<TEntity> _table;

        public DapperRepository(DapperDatabase database)
        {
            if (database == null) throw new ArgumentNullException("database");
            _database = database;
            _table = new DapperDatabase.Table<TEntity>(database, "unsupport");
        }

        public IEnumerable<TEntity> Select(long offset, long limit)
        {
            // TODO: implement partial fetching by database functionality
            return _table.All().Skip((int)offset).Take((int)limit);
        }

        public TEntity SelectOne(int id)
        {
            return _table.Get(id);
        }

        public bool InsertOrUpdate(TEntity entity)
        {
            int result = entity.Id != 0 ? _table.Update(entity.Id, entity) : _table.Insert(entity).GetValueOrDefault();
            return result > 0;
        }

        public bool Delete(TEntity entity)
        {
            if (entity.Id > 0)
                return _table.Delete(entity.Id);
            return false;
        }

        public void Commit()
        {
            _database.CommitTransaction();
        }

        public void Dispose()
        {
            _database.RollbackTransaction();
        }
    }
}