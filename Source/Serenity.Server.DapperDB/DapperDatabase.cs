﻿using Dapper;
using Serenity.Server.Data;
using Serenity.Server.Data.Entities;

namespace Serenity.Server.DapperDB
{
    public class DapperDatabase : Database<DapperDatabase>, IDatabase
    {
        public DapperDatabase()
        {
        }

        public IRepository<TEntity, int> GetRep<TEntity>() where TEntity : BaseEntity
        {
            return new DapperRepository<TEntity>(this);
        }
    }
}