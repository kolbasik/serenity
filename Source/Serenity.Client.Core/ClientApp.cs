﻿using System;
using System.Windows.Forms;
using Serenity.Client.Core.Views;

namespace Serenity.Client.Core
{
    public static class ClientApp
    {
        public static void Start(Uri serverUrl)
        {
            if (serverUrl == null) throw new ArgumentNullException("serverUrl");
            RunClient(serverUrl);
        }

        private static void RunClient(Uri serverUrl)
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new ShellForm(serverUrl));
        }
    }
}