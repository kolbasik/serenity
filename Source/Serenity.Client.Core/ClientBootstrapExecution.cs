﻿using System;
using Serenity.Foundation;

namespace Serenity.Client.Core
{
    public class ClientBootstrapExecution : IBootstrapExecution
    {
        private readonly IGlobalSettings _settings;

        public ClientBootstrapExecution(IGlobalSettings settings)
        {
            if (settings == null) throw new ArgumentNullException("settings");
            _settings = settings;
        }

        public void Execute()
        {
            ClientApp.Start(_settings.ServerUrl);
        }
    }
}