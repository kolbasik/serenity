﻿using System;
using System.Windows.Forms;

namespace Serenity.Client.Core.Views
{
    public partial class ShellForm : Form
    {
        public ShellForm(Uri url)
        {
            if (url == null) throw new ArgumentNullException("url");

            InitializeComponent();

            this.viewport.Navigate(url);
        }
    }
}
