﻿using System;
using System.ServiceModel;
using System.ServiceModel.Description;
using System.ServiceModel.Web;
using Autofac;
using Nancy.Hosting.Wcf;
using Serenity.Foundation;

namespace Serenity.Server.Core
{
    public static class ServerApp
    {
        public static volatile ILifetimeScope Container;
        public static volatile IGlobalSettings GlobalSettings;

        public static void Start(ILifetimeScope container)
        {
            if (container == null) throw new ArgumentNullException("container");
            Container = container;
            GlobalSettings = container.Resolve<IGlobalSettings>();
            RunServer(GlobalSettings.ServerUrl);
        }

        /*private static RunServer(string serverUrl)
        {
            var webServer = new WebServer(request => string.Format("<html><body>Time: {0}</body></html>", DateTime.Now), serverUrl);
            webServer.Run();
        }*/

        private static void RunServer(Uri serverUrl)
        {
            var whb = new WebHttpBinding(WebHttpSecurityMode.TransportCredentialOnly);
            whb.Security.Transport.ClientCredentialType = HttpClientCredentialType.None;
            var host = new WebServiceHost(new NancyWcfGenericService(), serverUrl);
            host.AddServiceEndpoint(typeof(NancyWcfGenericService), whb, "");
            host.Description.Behaviors.Add(new ServiceMetadataBehavior { HttpGetEnabled = true });
            host.Open();
        }
    }
}