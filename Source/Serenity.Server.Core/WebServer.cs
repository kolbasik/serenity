﻿using System;
using System.Diagnostics;
using System.Net;
using System.Text;
using System.Threading;

namespace Serenity.Server.Core
{
    // credits for this class go to David
    // http://www.codehosting.net/blog/BlogEngine/post/Simple-C-Web-Server.aspx
    public class WebServer : IDisposable
    {
        private readonly HttpListener _listener = new HttpListener();
        private readonly Func<HttpListenerRequest, string> _responderMethod;
 
        public WebServer(string[] prefixes, Func<HttpListenerRequest, string> method)
        {
            if (!HttpListener.IsSupported)
                throw new NotSupportedException("Needs Windows XP SP2, Server 2003 or later.");
 
            // URI prefixes are required, for example 
            // "http://localhost:8080/index/".
            if (prefixes == null || prefixes.Length == 0)
                throw new ArgumentException("prefixes");
 
            // A responder method is required
            if (method == null)
                throw new ArgumentException("method");
 
            foreach (string s in prefixes)
                _listener.Prefixes.Add(s);
 
            _responderMethod = method;
            _listener.Start();
        }
 
        public WebServer(Func<HttpListenerRequest, string> method, params string[] prefixes)
            : this(prefixes, method) { }
 
        public void Run()
        {
            ThreadPool.QueueUserWorkItem(obj =>
            {
                Trace.WriteLine("WebServer running...");
                try
                {
                    while (_listener.IsListening)
                    {
                        ThreadPool.QueueUserWorkItem(state =>
                        {
                            var ctx = (HttpListenerContext) state;
                            try
                            {
                                string response = _responderMethod(ctx.Request);
                                byte[] buf = Encoding.UTF8.GetBytes(response);
                                ctx.Response.ContentLength64 = buf.Length;
                                ctx.Response.OutputStream.Write(buf, 0, buf.Length);
                            }
                            catch (Exception ex)
                            {
                                Trace.TraceError(ex.Message);
                            } 
                            finally
                            {
                                ctx.Response.OutputStream.Close();
                            }
                        }, _listener.GetContext());
                    }
                }
                catch (Exception ex)
                {
                    Trace.TraceError(ex.Message);
                } 
            });
        }
 
        public void Stop()
        {
            Trace.WriteLine("WebServer stopping...");
            _listener.Stop();
            _listener.Close();
        }

        void IDisposable.Dispose()
        {
            Stop();
        }
    }
}
