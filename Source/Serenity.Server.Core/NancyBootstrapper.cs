using Autofac;
using Nancy.Bootstrapper;
using Nancy.Bootstrappers.Autofac;
using Nancy.Diagnostics;
using Nancy.Responses;
using Nancy.Security;
using Serenity.Foundation;

namespace Serenity.Server.Core
{
    public sealed class NancyBootstrapper : AutofacNancyBootstrapper
    {
        protected override ILifetimeScope GetApplicationContainer()
        {
            return ServerApp.Container ?? base.GetApplicationContainer();
        }

        protected override void ConfigureApplicationContainer(ILifetimeScope container)
        {
            // we should use Newtonsoft.Json
            InternalConfiguration.Serializers.Remove(typeof (DefaultJsonSerializer));
        }

        protected override void ApplicationStartup(ILifetimeScope container, IPipelines pipelines)
        {
            Csrf.Enable(pipelines);
        }

        protected override DiagnosticsConfiguration DiagnosticsConfiguration
        {
            get { return new DiagnosticsConfiguration { Password = ApplicationContainer.Resolve<IGlobalSettings>().Password }; }
        }
    }
}