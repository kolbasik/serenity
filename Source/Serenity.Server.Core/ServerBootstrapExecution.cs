﻿using System;
using Autofac;
using Serenity.Foundation;

namespace Serenity.Server.Core
{
    public class ServerBootstrapExecution : IBootstrapExecution
    {
        private readonly ILifetimeScope _container;

        public ServerBootstrapExecution(ILifetimeScope container)
        {
            if (container == null) throw new ArgumentNullException("container");
            _container = container;
        }

        public void Execute()
        {
            ServerApp.Start(_container);
        }
    }
}