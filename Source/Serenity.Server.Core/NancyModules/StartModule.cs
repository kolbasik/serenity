using Nancy;

namespace Serenity.Server.Core.NancyModules
{
    public class StartModule : NancyModule
    {
        public StartModule()
        {
            Get["/"] = parameters => View["Index.sshtml"];
        }
    }
}