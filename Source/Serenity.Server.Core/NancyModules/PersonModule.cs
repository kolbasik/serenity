using Serenity.Server.Data;
using Serenity.Server.Data.Entities;

namespace Serenity.Server.Core.NancyModules
{
    public class PersonModule : RestModule<PersonEntity>
    {
        public PersonModule(IUnityOfWork unityOfWork)
            : base(unityOfWork, "/v1/person")
        {
        }

        protected override void Bind(PersonEntity source, PersonEntity target)
        {
            target.FirstName = source.FirstName ?? string.Empty;
            target.LastName = source.LastName ?? string.Empty;
        }
    }
}