using System;
using Nancy;

namespace Serenity.Server.Core.NancyModules
{
    public class AdminModule : NancyModule
    {
        public AdminModule()
            : base("/admin")
        {
            Get["/ping"] = parameters => DateTime.UtcNow.ToUniversalTime().ToLongTimeString();
            Get["/test-rest"] = parameters => View["TestRest.sshtml"];
        }
    }
}