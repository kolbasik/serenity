using Newtonsoft.Json;

namespace Serenity.Server.Core.NancyModules
{
    public class SuccessRestResult : RestResult
    {
        public SuccessRestResult(object data)
            : base(true)
        {
            this.Data = data;
        }

        [JsonProperty("data")]
        public object Data { get; private set; }
    }
}