using System;
using System.Collections.Generic;
using System.IO;
using Nancy;
using Nancy.ViewEngines;

namespace Serenity.Server.Core.NancyModules
{
    public class TemplateModule : NancyModule
    {
        public TemplateModule()
            : base("/template")
        {
            Get["/js"] = parameters => View["Index.sshtml", new TemplateDto(this)].WithContentType("application/javascript");
            Get["/{name}"] = parameters => View[parameters["name"] + ".sshtml", new TemplateDto(this)];
        }

        public string GetTemplate(string name, TemplateDto templateDto)
        {
            var viewContext = new ViewLocationContext { Context = this.Context, ModulePath = this.ModulePath };
            var response = ViewFactory.RenderView(name, templateDto, viewContext);
            using (var stream = new MemoryStream())
            {
                response.Contents(stream);
                stream.Position = 0;
                var template = new StreamReader(stream).ReadToEnd();
                return template;
            }
        }
    }

    public class TemplateDto : DynamicDictionary, IDictionary<string, object>
    {
        private readonly TemplateModule _module;

        public TemplateDto(TemplateModule module)
        {
            _module = module;
        }

        bool IDictionary<string, object>.TryGetValue(string key, out object value)
        {
            value = _module.GetTemplate(key, this).Replace(Environment.NewLine, "").Replace("'", "\"");
            return true;
        }
    }
}