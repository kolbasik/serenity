using Newtonsoft.Json;

namespace Serenity.Server.Core.NancyModules
{
    public abstract class RestResult
    {
        protected RestResult(bool ok)
        {
            this.Ok = ok;
        }

        [JsonProperty("ok")]
        public bool Ok { get; private set; }
    }
}