using System;
using System.Collections.Generic;
using Nancy;
using Nancy.ModelBinding;
using Serenity.Server.Data;
using Serenity.Server.Data.Entities;

namespace Serenity.Server.Core.NancyModules
{
    public abstract class RestModule<TEntity> : NancyModule
        where TEntity : BaseEntity, new()
    {
        protected readonly IUnityOfWork UnityOfWork;

        protected RestModule(IUnityOfWork unityOfWork, string modulePath)
            : base(modulePath)
        {
            if (unityOfWork == null) throw new ArgumentNullException("unityOfWork");
            UnityOfWork = unityOfWork;

            //this.RequiresAuthentication();
            //this.ValidateCsrfToken(TimeSpan.FromDays(1));

            base.Get["/"] = parameters => Handle(() => this.GetAll(parameters));
            base.Get["/{id}"] = parameters => Handle(() => this.GetOne(parameters));
            base.Post["/"] = parameters => Handle(() => this.Create(parameters), HttpStatusCode.Created);
            base.Put["/{id}"] = parameters => Handle(() => this.Update(parameters));
            base.Delete["/{id}"] = parameters => Handle(() => this.Delete(parameters));
            base.Patch["/{id}"] = parameters => Handle(() => this.Patch(parameters));
        }

        protected virtual IEnumerable<TEntity> GetAll(dynamic parameters)
        {
            using (var db = UnityOfWork.UseOnceTo())
            {
                return db.GetRep<TEntity>().Select(0, 25);
            }
        }

        protected virtual TEntity GetOne(dynamic parameters)
        {
            var id = GetId(parameters);
            using (var db = UnityOfWork.UseOnceTo())
            {
                var origin = db.GetRep<TEntity>().SelectOne(id);
                if (origin == null)
                    throw new KeyNotFoundException("Object not found in the storage!");
                return origin;
            }
        }

        protected virtual bool Create(dynamic parameters)
        {
            using (var db = UnityOfWork.UseOnceTo())
            {
                var table = db.GetRep<TEntity>();
                var origin = new TEntity();
                return origin != null && this.BindTo(origin) && table.InsertOrUpdate(origin);
            }
        }

        protected virtual bool Update(dynamic parameters)
        {
            var id = GetId(parameters);
            using (var db = UnityOfWork.UseOnceTo())
            {
                var table = db.GetRep<TEntity>();
                var origin = table.SelectOne(id);
                return origin != null && this.BindTo(origin) && table.InsertOrUpdate(origin);
            }
        }

        protected virtual bool Delete(dynamic parameters)
        {
            var id = GetId(parameters);
            using (var db = UnityOfWork.UseOnceTo())
            {
                var table = db.GetRep<TEntity>();
                var origin = table.SelectOne(id);
                return origin != null && table.Delete(origin);
            }
        }

        protected virtual dynamic Patch(dynamic parameters)
        {
            throw new NotSupportedException();
        }

        protected virtual void Bind(TEntity source, TEntity target)
        {
        }

        private bool BindTo(TEntity target)
        {
            var bindingConfig = new BindingConfig { BodyOnly = true };
            var source = this.Bind<TEntity>(bindingConfig, x => x.Id, x => x.CreatedOn, x => x.ModifiedOn, x => x.IsActive);
            Bind(source, target);
            return true;
        }

        private int GetId(dynamic parameters)
        {
            return Convert.ToInt32((string)parameters["id"]);
        }

        private object Handle(Func<object> execute, HttpStatusCode succesStatusCode = HttpStatusCode.OK)
        {
            object result;
            try
            {
                var state = execute();
                // result = new SuccessRestResult(state);
                result = state;
            }
            catch (Exception ex)
            {
                result = new ErrorRestResult(ex.Message);
            }
            var statusCode = result is ErrorRestResult ? HttpStatusCode.BadRequest : succesStatusCode;
            return this.Response.AsJson(result, statusCode);
        }
    }
}