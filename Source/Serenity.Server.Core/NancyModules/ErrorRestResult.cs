using Newtonsoft.Json;

namespace Serenity.Server.Core.NancyModules
{
    public class ErrorRestResult : RestResult
    {
        public ErrorRestResult(string message)
            : base(false)
        {
            this.Message = message;
        }

        [JsonProperty("message")]
        public string Message { get; private set; }
    }
}