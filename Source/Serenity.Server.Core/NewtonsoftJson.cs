﻿using System;
using System.Collections.Generic;
using System.IO;
using Nancy;
using Nancy.IO;
using Newtonsoft.Json;

namespace Serenity.Server.Core
{
    public class NewtonsoftJsonSerializer : ISerializer
    {
        public bool CanSerialize(string contentType)
        {
            return IsJsonType(contentType);
        }

        public IEnumerable<string> Extensions
        {
            get { yield return "json"; }
        }

        public void Serialize<TModel>(string contentType, TModel model, Stream outputStream)
        {
            using (var writer = new StreamWriter(new UnclosableStreamWrapper(outputStream)))
            {
                var serializer = new JsonSerializer();
                serializer.Serialize(writer, model);
            }
        }

        private static bool IsJsonType(string contentType)
        {
            if (string.IsNullOrEmpty(contentType))
            {
                return false;
            }

            var contentMimeType = contentType.Split(';')[0];

            return contentMimeType.Equals("application/json", StringComparison.InvariantCultureIgnoreCase) ||
                   contentMimeType.StartsWith("application/json-", StringComparison.InvariantCultureIgnoreCase) ||
                   contentMimeType.Equals("text/json", StringComparison.InvariantCultureIgnoreCase) ||
                  (contentMimeType.StartsWith("application/vnd", StringComparison.InvariantCultureIgnoreCase) &&
                   contentMimeType.EndsWith("+json", StringComparison.InvariantCultureIgnoreCase));
        }
    }
}