﻿using System;

namespace Serenity.Client.Desktop
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            new Bootstrap().Run();
        }
    }
}
