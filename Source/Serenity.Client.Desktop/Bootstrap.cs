﻿using System.Collections.Generic;
using System.Globalization;
using System.Threading;
using Autofac;
using Autofac.Configuration;
using Serenity.Foundation;

namespace Serenity.Client.Desktop
{
    public class Bootstrap
    {
        private IContainer Container { get; set; }

        public void Run()
        {
            InitializeCulture();
            InitializeIoc();
            RunBootstrapExecutions();
        }

        private static void InitializeCulture()
        {
            var thread = Thread.CurrentThread;
            var culture = CultureInfo.InvariantCulture;
            thread.CurrentCulture = culture;
            thread.CurrentUICulture = culture;
            CultureInfo.DefaultThreadCurrentCulture = culture;
            CultureInfo.DefaultThreadCurrentUICulture = culture;
        }

        private void InitializeIoc()
        {
            var builder = new ContainerBuilder();
            builder.RegisterModule(new ConfigurationSettingsReader("autofac"));
            Container = builder.Build();
        }

        private void RunBootstrapExecutions()
        {
            using (var scope = Container.BeginLifetimeScope())
            {
                var executions = scope.Resolve<IEnumerable<IBootstrapExecution>>();
                foreach (var execution in executions)
                {
                    execution.Execute();
                }
            }
        }
    }
}