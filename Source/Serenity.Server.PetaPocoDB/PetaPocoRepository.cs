﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using PetaPoco;
using Serenity.Server.Data;
using Serenity.Server.Data.Entities;

namespace Serenity.Server.PetaPocoDB
{
    public class PetaPocoRepository<TEntity> : IRepository<TEntity, int>
        where TEntity : BaseEntity
    {
        private readonly PetaPocoDatabase _database;

        public PetaPocoRepository(PetaPocoDatabase database)
        {
            if (database == null) throw new ArgumentNullException("database");
            _database = database;

            TableName = string.Format("dbo.{0}s", Regex.Replace(typeof (TEntity).Name, "Entity$", ""));
            PrimaryKeyName = "Id";
        }

        private string TableName { get; set; }
        private string PrimaryKeyName { get; set; }

        public IEnumerable<TEntity> Select(long offset, long limit)
        {
            var sql = Sql.Builder.Select("*").From(this.TableName);
            var page = offset/limit+1;
            return _database.Page<TEntity>(page, limit, sql).Items;
        }

        public TEntity SelectOne(int id)
        {
            var sql = Sql.Builder
                        .Select("*")
                        .From(this.TableName)
                        .Where(string.Format("{0}=@0", this.PrimaryKeyName), id);
            return _database.SingleOrDefault<TEntity>(sql);
        }

        public bool InsertOrUpdate(TEntity entity)
        {
            bool result;
            if (entity.Id < 1)
            {
                entity.CreatedOn = DateTime.UtcNow;
                entity.ModifiedOn = entity.CreatedOn;
                result = _database.Insert(TableName, PrimaryKeyName, true, entity) != null;
            }
            else
            {
                entity.ModifiedOn = DateTime.UtcNow;
                result = _database.Update(TableName, PrimaryKeyName, entity, entity.Id) > 0;
            }
            return result;
        }

        public bool Delete(TEntity entity)
        {
            return _database.Delete(TableName, PrimaryKeyName, entity, entity.Id) > 0;
        }

        public void Commit()
        {
            _database.CompleteTransaction();
        }

        public void Dispose()
        {
            _database.AbortTransaction();
        }
    }
}