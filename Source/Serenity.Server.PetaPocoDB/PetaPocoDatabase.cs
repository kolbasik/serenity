﻿using System;
using System.Data;
using PetaPoco;
using Serenity.Server.Data;
using Serenity.Server.Data.Entities;

namespace Serenity.Server.PetaPocoDB
{
    public sealed class PetaPocoDatabase : Database, IDatabase
    {
        public PetaPocoDatabase(string connectionString, string providerName)
            : base(connectionString, providerName)
        {
        }

        public IRepository<TEntity, int> GetRep<TEntity>() where TEntity : BaseEntity
        {
            return new PetaPocoRepository<TEntity>(this);
        }

        void IDatabase.BeginTransaction(IsolationLevel isolation)
        {
            this.BeginTransaction(isolation);
        }

        void IDatabase.CommitTransaction()
        {
            this.CompleteTransaction();
        }

        void IDatabase.RollbackTransaction()
        {
            this.AbortTransaction();
        }

        void IDisposable.Dispose()
        {
            this.AbortTransaction();
        }
    }
}