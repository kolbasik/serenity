﻿using System.Configuration;
using Serenity.Server.Data;

namespace Serenity.Server.PetaPocoDB
{
    public class PetaPocoDatabaseFactory : IDatabaseFactory
    {
        public IDatabase CreateDatabase()
        {
            var settings = ConfigurationManager.ConnectionStrings["petapoco"];
            var database = new PetaPocoDatabase(settings.ConnectionString, settings.ProviderName);
            return database;
        }
    }
}