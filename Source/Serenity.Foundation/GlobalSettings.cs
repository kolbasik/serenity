﻿using System;
using System.Configuration;

namespace Serenity.Foundation
{
    public class GlobalSettings : IGlobalSettings
    {
        private const string DefaultServerUrl = "http://localhost:8088/";

        public GlobalSettings()
        {
            ServerUrl = new Uri(ConfigurationManager.AppSettings["serverUrl"] ?? DefaultServerUrl);
            Password = ConfigurationManager.AppSettings["nancyPassword"] ?? "@dm1n!";
        }
        public Uri ServerUrl { get; private set; }
        public string Password { get; private set; }
    }
}