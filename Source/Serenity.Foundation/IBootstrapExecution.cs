﻿namespace Serenity.Foundation
{
    public interface IBootstrapExecution
    {
        void Execute();
    }
}
