﻿using System;

namespace Serenity.Foundation
{
    public interface IGlobalSettings
    {
        Uri ServerUrl { get; }
        string Password { get; }
    }
}